

## Okay, we have some code to refactoring

```php
<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller {

    public function update($book_id, Request $request)
    {
        $book = Book::find($book_id);
        if(!$book) {
            abort(404);
        }

        $validator = Validator::make($request->all(), [
            'book_text' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $book->update($request->all());

        foreach ($book->authors as $author) {
            $words_count = 0;
            foreach ($author->books as $books) {
                $words_count += str_word_count($books->book_text);
            }
            $author->words_count = $words_count;
            $author->save();
        }

        $admin = User::where('role', 'admin')->first();
        if(!$admin) {
            Mail::send('emails.book_updated', ['book' => $book], function ($m) use ($admin) {
                $m->from('hello@book.com', 'Your App');
                $m->to($admin->email, $admin->name)->subject('Book updated');
            })
        }

        return redirect()->route('admin.books.index');
    }
}

?>

```
