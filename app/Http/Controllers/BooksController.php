<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookUpdateRequest;
use App\Models\Book;

class BooksController extends Controller {

    public function update(Book $book, BookUpdateRequest $request)
    {
        $book->update($request->all());
        return redirect()->route('admin.books.index');
    }
}
