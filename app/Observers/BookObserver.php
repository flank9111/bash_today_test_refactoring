<?php

namespace App\Observers;

use App\Jobs\SendNotificationBookUpdatedJob;
use App\Models\Book;
use App\Services\BookService;

class BookObserver
{
    /**
     * Handle the Book "created" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function created(Book $book)
    {
        //
    }

    /**
     * Handle the Book "updated" event.
     *
     * @param Book $book
     * @param BookService $bookService
     * @return void
     */
    public function updated(Book $book, BookService $bookService)
    {
        foreach ($book->authors as $author) {
            $author->words_count = $bookService->computingAuthorWords($author);
            $author->save();
        }
        dispatch(new SendNotificationBookUpdatedJob($book));
    }

    /**
     * Handle the Book "deleted" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function deleted(Book $book)
    {
        //
    }

    /**
     * Handle the Book "restored" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function restored(Book $book)
    {
        //
    }

    /**
     * Handle the Book "force deleted" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function forceDeleted(Book $book)
    {
        //
    }
}
