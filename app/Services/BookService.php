<?php

namespace App\Services;

class BookService
{
    public function computingAuthorWords($author)
    {
        $words_count = 0;
        foreach ($author->books as $books) {
            $words_count += str_word_count($books->book_text);
        }
        return $words_count;
    }
}
